cmake_minimum_required(VERSION 3.15)
project(supermarketTest)

set(CMAKE_CXX_STANDARD 14)

add_subdirectory(supermarketTest)