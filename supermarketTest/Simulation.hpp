#pragma once


#include "Shop.hpp"
#include  <thread>


class Simulation {

private:
    int duration;
    int clientsPerTick;
    int tick;
    int lastTick;
    thread t1;
    Shop shop;
public:
    Simulation() {
        duration = 10;
        tick = 0;
        clientsPerTick=1;
    };

/*
public:
    void start() {
        t1 = std::thread([this] { this->metronome(this->duration); });
        simulate();
        t1.join();
        //t1.detach();

    }

private:

    void metronome(int duration) {
        for (int i = 0; i < duration; i++) {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            tick++;
        }
    }
*/

public:
    void start(){
        for(int i =0;i< duration;i++)
        {
            std::this_thread::sleep_for(std::chrono::seconds(5));
            simulate();

        }
    }

    void simulate(){

        int prefferedCashRegister  =0;

        for (int i = 0; i < clientsPerTick ; i++) {
            Customer customer = Customer();
            shop.addCustomer(customer);
        }

        for(auto customer : shop.getClients()){
            if(customer.isReadyToCash()){
                for(auto cashRegister : shop.getCashRegisters()){
                    if(cashRegister.queueSize()<=shop.getCashRegisters().at(prefferedCashRegister).queueSize()){

                    }
                }
                shop.queueCustomer(customer,shop.getCashRegisters().at(prefferedCashRegister));
            }
        }

    }
};