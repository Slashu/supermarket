#pragma once

#include <string>
#include <ostream>

using namespace std;

class Product {
public:
    Product();

    Product(const string &productName, double productPrice);

    const string &getProductName() const;

    void setProductName(const string &productName);

    double getProductPrice() const;

    void setProductPrice(double productPrice);

    friend ostream &operator<<(ostream &os, const Product &product);

private:
    string productName;
    double productPrice;
};


Product::Product(const string &productName, double productPrice) : productName(productName),
                                                                   productPrice(productPrice) {}

const string &Product::getProductName() const {
    return productName;
}

void Product::setProductName(const string &productName) {
    Product::productName = productName;
}

double Product::getProductPrice() const {
    return productPrice;
}

void Product::setProductPrice(double productPrice) {
    Product::productPrice = productPrice;
}

ostream &operator<<(ostream &os, const Product &product) {
    os << "product name: " << product.productName << " product price: " << product.productPrice;
    return os;
}


