#include <iostream>
#include "Inventory.hpp"
#include <vector>
#include <iterator>

#include "Simulation.hpp"
#include "Customer.hpp"

using namespace std;

int main() {
    /*vector<Product> list;
    Inventory inventory = Inventory();
    list = inventory.getProductList();
    vector<Product>::iterator it;
    for (it = list.begin(); it != list.end(); it) {
        cout << *it << endl;
        it++;
        }*/
    Simulation simulation = Simulation();
    simulation.start();
    return 0;
}
