#pragma once

#include "Person.hpp"
#include "Product.hpp"
#include <vector>
#include <random>
#include <chrono>
#include <iostream>

class Customer{

private:
    int shoppingListSize;
    default_random_engine randomEngine;
    bool readyToCash = false;
    vector<Product> cart;

public:
    Customer() {
        initialize();
    };

private:
    void initialize() {
        randomEngine.seed(chrono::system_clock::now().time_since_epoch().count());
        uniform_int_distribution<int> distribution(2, 10);
        shoppingListSize = distribution(randomEngine);
    }

private:
    void addProduct(Product product) {
        cart.push_back(product);
        if (cart.size() == shoppingListSize) {
            readyToCash = true;
        }
    }

public:
    bool isReadyToCash() const {
        return readyToCash;
    }

    const vector<Product> &getCart() const {
        return cart;
    }
};

