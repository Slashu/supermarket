#pragma once

#include "Person.hpp"

class Cashier {
private:
    int speed;

public:
    Cashier() {
    }

    Cashier(int speed){
        this->speed=speed;
    }

    int getSpeed() const {
        return speed;
    }

    void setSpeed(int speed) {
        Cashier::speed = speed;
    }

};
