#pragma once

#include <deque>
#include "Customer.hpp"
#include "Cashier.hpp"
#include <thread>

using namespace std;


class CashRegister {
private:
    bool isOpen = true;
    deque<Customer> queue;
    Cashier cashier;
    int prefferedIndex = 0;

public:
    CashRegister() {};

    CashRegister(Cashier cashier) {

    }


public:
    void addCustomer(Customer customer) {
        queue.push_back(customer);
    }

    void processCustomer() {
        queue.front();
    }

    int queueSize() {
        return queue.size();
    }

    void open() {
        isOpen = true;
    }

    void close() {
        isOpen = false;
    }

    bool checkIfOpen() const {
        return isOpen;
    }

    void reset() {
        queue.clear();
    }


};

