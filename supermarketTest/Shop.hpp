#pragma once

#include "Customer.hpp"
#include "CashRegister.hpp"

class Shop {

private:
    vector<Customer> clients;
    vector<CashRegister> cashRegisters;
    default_random_engine randomEngine;
    int prefferedCashRegister;

public:
    Shop() {
        initialize();

    }

    void addCustomer(Customer customer){
        clients.push_back(customer);
    }

    void addCashRegister(CashRegister cashRegister){
        cashRegisters.push_back(cashRegister);
    }

    void openCashRegister(CashRegister cashRegister){
        cashRegister.open();
    }
    void closeCashRegister(CashRegister cashRegister){
        cashRegister.close();
    }

    void queueCustomer(Customer customer,CashRegister cashRegister){
        cashRegister.addCustomer(customer);
    }

    const vector<Customer> &getClients() const {
        return clients;
    }

    const vector<CashRegister> &getCashRegisters() const {
        return cashRegisters;
    }

private:
    void initialize(){
        randomEngine.seed(chrono::system_clock::now().time_since_epoch().count());
        uniform_int_distribution<int> distribution(1, 3);
        for (int i = 0; i < 6; ++i) {
            Cashier cashier = Cashier(distribution(randomEngine));
            addCashRegister(cashier);
        }
    }
};