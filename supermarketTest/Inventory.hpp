#pragma once

#include <ostream>
#include <vector>
#include "Product.hpp"


class Inventory {

public:
    Inventory() {
        initialize();
    }

    vector<Product> productList;

    void initialize() {
        Product bread = Product("bread", 2.97);
        Product butter = Product("butter", 3.85);
        Product cheese = Product("cheese", 2.75);
        Product ham = Product("ham", 5.5);
        addToList(bread);
        addToList(butter);
        addToList(cheese);
        addToList(ham);
    };

    void addToList(Product product) {
        this->productList.push_back(product);
    }

    const vector<Product> &getProductList() const {
        return productList;
    }

};